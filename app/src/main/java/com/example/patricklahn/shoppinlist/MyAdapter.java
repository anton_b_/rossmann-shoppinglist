package com.example.patricklahn.shoppinlist;


import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.patricklahn.shoppinlist.Db.DaoSession;
import com.example.patricklahn.shoppinlist.Db.Grocery;

import java.util.ArrayList;
import java.util.List;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private static final String TAG = "MyAdapter";
    private Context context;
    List<Grocery> groceries;
    private List<Long> selectedGroceries = new ArrayList<>();
    private DaoSession daoSession;
    public long itemToBeRemovedId = 0;

    // Provide a suitable constructor (depends on the kind of dataset)
    public MyAdapter(Context context, List<Grocery> groceries, DaoSession daoSession) {
        this.context = context;
        this.groceries = groceries;
        this.daoSession = daoSession;

        Log.d(TAG, "MyAdapter   constructor: ");
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {


        // create a new view
        RelativeLayout v = (RelativeLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.grocery_list_row, parent, false);
        return new MyViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder   Position: " + Integer.toString(position));

        // - get element from your dataset at this position
        final Grocery grocery = groceries.get(position);
        // - replace the contents of the view with that element
        holder.quantity.setText(Integer.toString(grocery.getQuantity()) + " x ");
        holder.name.setText(grocery.getName());
//        holder.name.setText(grocery.getName());
        holder.ean.setText("EAN: " + grocery.getEan());
////        holder.update(groceries.get(position));

        holder.relativeLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (MainActivity.mActionMode != null) {
                    return false;
                }

                // Start the CAB using the ActionMode.Callback defined above
                MainActivity.mActionMode = ((AppCompatActivity)context).startActionMode((ActionMode.Callback) context);
                view.setSelected(true);
                itemToBeRemovedId = groceries.get(position).getId();
                return true;
            }
        });

        holder.btnIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    Log.d(TAG , "onClick increase");
                    grocery.setQuantity(grocery.getQuantity()+1);
                    daoSession.getGroceryDao().update(grocery);

                    notifyDataSetChanged();
                }
            }
        });
        holder.btnDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                {
                    Log.d(TAG , "onClick decrease");
                    if (grocery.getQuantity()>0)
                    {
                        grocery.setQuantity(grocery.getQuantity()-1);
                        daoSession.getGroceryDao().update(grocery);

                        notifyDataSetChanged();
                    }

                }
            }
        });
        long id = grocery.getId();

        if (selectedGroceries.contains(grocery)) {
            //if item is selected then,set foreground color of FrameLayout.
            holder.relativeLayout.setForeground(new ColorDrawable(ContextCompat.getColor(context, android.R.color.background_dark)));
        } else {
            //else remove selected item color.
            holder.relativeLayout.setForeground(new ColorDrawable(ContextCompat.getColor(context, android.R.color.transparent)));
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        Log.d(TAG, "MyAdapter   getItemCount:  " + Integer.toString(groceries.size()));
        return groceries.size();
    }

    public Grocery getItem(int position) {
        return groceries.get(position);
    }

    public void setSelectedGroceries(List<Long> selectedGroceries) {
        this.selectedGroceries = selectedGroceries;
        notifyDataSetChanged();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class MyViewHolder extends RecyclerView.ViewHolder {

        // each data item is just a string in this case
        TextView quantity, name, ean;
        Button btnIncrease, btnDecrease;
        RelativeLayout relativeLayout;


        MyViewHolder(RelativeLayout v) {
            super(v);

            quantity = v.findViewById(R.id.list_quantity);
            name = itemView.findViewById(R.id.list_name);
            ean = itemView.findViewById(R.id.list_ean);
            btnIncrease = itemView.findViewById(R.id.button_increase_quantity);
            btnDecrease = itemView.findViewById(R.id.button_decrease_quantity);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);


        }
    }
}
