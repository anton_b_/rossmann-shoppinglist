package com.example.patricklahn.shoppinlist;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.patricklahn.shoppinlist.Db.DaoSession;
import com.example.patricklahn.shoppinlist.Db.Grocery;
import com.example.patricklahn.shoppinlist.Db.GroceryDao;
import com.example.patricklahn.shoppinlist.JSON.FetchData;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ActionMode.Callback {

    List<Grocery> groceries = new ArrayList<>();

    public DaoSession daoSession;
    ArrayAdapter<Grocery> groceryArrayAdapter;

    private EditText name, quantity;
    private Button btn_save;
    private RecyclerView mRecyclerView;
    private MyAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private GroceryDao groceryDao;

    public static ActionMode mActionMode;
    private Button btnScan;
    public String eanCodeString;
    public static Grocery fullGrocery;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        daoSession = ((AppController) getApplication()).getDaoSession();

        setupListView();
        activateAddButton();
        activateScanButton();
    }

    @Override
    protected void onResume() {
        super.onResume();

        setupListView();
    }

    private void setupListView() {
        name = (EditText) findViewById(R.id.editText_product);
        quantity = (EditText) findViewById(R.id.editText_quantity);
        btn_save = (Button) findViewById(R.id.button_add_product);
        btnScan = (Button) findViewById(R.id.btn_scan);
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

//        listView = (ListView) findViewById(R.id.listview);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);


        groceries = daoSession.getGroceryDao().loadAll();

        // specify an adapter (see also next example)
        mAdapter = new MyAdapter(this, groceries, daoSession);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mRecyclerView.setLayoutManager(mLayoutManager);
        // set the adapter
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mRecyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.VERTICAL, 16));
        mAdapter.notifyDataSetChanged();
    }


    private void activateAddButton() {
        Button buttonAddProduct = (Button) findViewById(R.id.button_add_product);
        final EditText editTextQuantity = (EditText) findViewById(R.id.editText_quantity);
        final EditText editTextProduct = (EditText) findViewById(R.id.editText_product);

        buttonAddProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                groceryDao = daoSession.getGroceryDao();
                Grocery grocery = new Grocery();

                String quantityString = String.valueOf(editTextQuantity.getText());
                String productString = editTextProduct.getText().toString();

                if (TextUtils.isEmpty(productString)) {
                    editTextProduct.setError(getString(R.string.editText_errorMessage));
                    return;
                }
                grocery.setQuantity(Integer.parseInt(quantityString));
                grocery.setName(String.valueOf(productString));
                grocery.setEan(null);
                grocery.setPicture(null);
                groceryDao.insert(grocery);
                Log.d("Dao", "Inserted new grocery, ID" + grocery.getId());
                editTextProduct.setText("");
                editTextQuantity.setText("");
                InputMethodManager inputManager = (InputMethodManager)
                        getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);
                setupListView();
            }

        });

    }

    protected void activateScanButton() {
        final Activity activity = this;
        btnScan = (Button) findViewById(R.id.btn_scan);
        btnScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IntentIntegrator integrator = new IntentIntegrator(activity);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.PRODUCT_CODE_TYPES);
                integrator.setPrompt("Scan");
                integrator.setCameraId(0);
                integrator.setBeepEnabled(false);
                integrator.setBarcodeImageEnabled(false);
                integrator.initiateScan();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Toast.makeText(this, "You cancelled the scanning", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, result.getContents(), Toast.LENGTH_SHORT).show();
                eanCodeString = result.getContents();
                insertEANIntoDB(eanCodeString);
            }
        } else {

            super.onActivityResult(requestCode, resultCode, data);

        }

    }

    public void insertEANIntoDB(String EANCodeString) {
        groceryDao = daoSession.getGroceryDao();
        Grocery grocery = new Grocery();
        grocery.setQuantity(1);
        grocery.setName("Test");
        grocery.setEan(EANCodeString);
        grocery.setPicture(null);

        int sameEan = groceryDao.queryBuilder()
                .where(GroceryDao.Properties.Ean.eq(grocery.getEan()))
                .list().size();

        if(sameEan > 0){
            Toast.makeText(this, "EAN bereits vorhanden", Toast.LENGTH_SHORT).show();
        } else {
            //TODO FetchData mit grocery.getEan
            FetchData process = new FetchData(grocery, groceryDao);
            process.execute();

        }

        setupListView();

    }

    void updateItem(long id) {
        GroceryDao groceryDao = daoSession.getGroceryDao();
        Grocery grocery = new Grocery();
        grocery.setId(id);
        grocery.setName(name.getText().toString());
        grocery.setQuantity(Integer.parseInt(quantity.getText().toString()));
        groceryDao.saveInTx(grocery);
        Toast.makeText(this, "Item updated", Toast.LENGTH_SHORT).show();
        finish();
    }

    private void deleteGroceryItem(long id) {
        //// Get the entity dao we need to work with.
        GroceryDao groceryDao = daoSession.getGroceryDao();
        /// perform delete operation
        groceryDao.deleteByKey(id);

        setupListView();
    }

    // Called when the action mode is created; startActionMode() was called
    @Override
    public boolean onCreateActionMode(ActionMode mode, Menu menu) {
        // Inflate a menu resource providing context menu items
        MenuInflater inflater = mode.getMenuInflater();
        inflater.inflate(R.menu.menu_contextual_action_bar, menu);
        return true;
    }

    // Called each time the action mode is shown. Always called after onCreateActionMode, but
    // may be called multiple times if the mode is invalidated.
    @Override
    public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
        return false; // Return false if nothing is done
    }

    // Called when the user selects a contextual menu item
    @Override
    public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.cab_delete:
                //TODO delete products

                if (mAdapter.itemToBeRemovedId != 0) {

                    deleteGroceryItem(mAdapter.itemToBeRemovedId);
                    mode.finish(); // Action picked, so close the CAB

                    mAdapter.itemToBeRemovedId = 0;
                }


                return true;
            default:
                return false;
        }
    }

    // Called when the user exits the action mode
    @Override
    public void onDestroyActionMode(ActionMode mode) {
        mActionMode = null;
    }
}