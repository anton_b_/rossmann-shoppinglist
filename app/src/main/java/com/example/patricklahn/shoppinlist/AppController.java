package com.example.patricklahn.shoppinlist;

import android.app.Application;

import com.example.patricklahn.shoppinlist.Db.DaoMaster;
import com.example.patricklahn.shoppinlist.Db.DaoSession;
import com.example.patricklahn.shoppinlist.Db.GroceryDao;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import org.greenrobot.greendao.database.Database;

import okhttp3.OkHttpClient;


public class AppController extends Application {

    public static final boolean ENCRYPTED = true;
    private DaoSession daoSession;

    @Override
    public void onCreate() {
        super.onCreate();


        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "grocery-db"); //The grocery-db here is the name of our database.
        Database db = helper.getWritableDb();
        daoSession = new DaoMaster(db).newSession();
        Stetho.initializeWithDefaults(this);
        new OkHttpClient.Builder()
                .addNetworkInterceptor(new StethoInterceptor())
                .build();
        // INSERT INTO NOTE (_id, DATE, TEXT) VALUES(1, 0, 'Example Note')
//        db.execSQL("INSERT INTO " + GroceryDao.TABLENAME + " (" +
//                GroceryDao.Properties.Id.columnName + ", " +
//                GroceryDao.Properties.Name.columnName + ", " +
//                GroceryDao.Properties.Ean.columnName + ", " +
//                GroceryDao.Properties.Quantity.columnName + ", " +
//                GroceryDao.Properties.Picture.columnName +
//                ") VALUES(3, 'Ketchup', '1234', '1', '1234')");
//        db.execSQL("INSERT INTO " + GroceryDao.TABLENAME + " (" +
//                GroceryDao.Properties.Id.columnName + ", " +
//                GroceryDao.Properties.Name.columnName + ", " +
//                GroceryDao.Properties.Ean.columnName + ", " +
//                GroceryDao.Properties.Quantity.columnName + ", " +
//                GroceryDao.Properties.Picture.columnName +
//                ") VALUES(4, 'Senf', '1234', '1', '1234')");
//        db.execSQL("INSERT INTO " + GroceryDao.TABLENAME + " (" +
//                GroceryDao.Properties.Id.columnName + ", " +
//                GroceryDao.Properties.Name.columnName + ", " +
//                GroceryDao.Properties.Ean.columnName + ", " +
//                GroceryDao.Properties.Quantity.columnName + ", " +
//                GroceryDao.Properties.Picture.columnName +
//                ") VALUES(2, 'Mayo', '1234', '1', '1234')");
    }

        public DaoSession getDaoSession () {
            return daoSession;
        }

    }