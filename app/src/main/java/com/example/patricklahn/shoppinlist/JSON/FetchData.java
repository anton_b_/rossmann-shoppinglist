package com.example.patricklahn.shoppinlist.JSON;

import android.os.AsyncTask;

import com.example.patricklahn.shoppinlist.Db.DaoSession;
import com.example.patricklahn.shoppinlist.Db.Grocery;
import com.example.patricklahn.shoppinlist.Db.GroceryDao;
import com.example.patricklahn.shoppinlist.MainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class FetchData extends AsyncTask<Void,Void,Void>{

    String data = "";
    private String dynUrl;
    private Grocery grocery;
    private GroceryDao groceryDao;


    public FetchData(Grocery grocery, GroceryDao groceryDao) {
        this.grocery = grocery;
        this.groceryDao = groceryDao;

        doInBackground();

    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            URL url = new URL("http://tbwvl-apawe08:10152/gtin.ws/gtin/4305615531472");

            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line = "";
            while (line != null){
                line = bufferedReader.readLine();
                data = data + line;
            }


            JSONObject JO = new JSONObject(data);
                grocery.setName((String) JO.get("name"));
                grocery.setEan((String) JO.get("ean"));
                grocery.setPicture((String) JO.get("picture"));
        } catch (java.io.IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);

        groceryDao.insert(grocery);
/*        MainActivity.data.setText(this.dataParsed);*/



    }
}

